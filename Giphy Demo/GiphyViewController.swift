import UIKit
import JellyGif

class GiphyViewController : UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
        
    @IBAction func buttonAction(_ sender: Any) {
        httpRequest() { url in
            let gifUrl = URL(string: url)!
            
            DispatchQueue.main.async {
                let jellyGifImageView = JellyGifImageView(frame: CGRect(x: 0, y: 0, width: 250, height: 150))
                jellyGifImageView.translatesAutoresizingMaskIntoConstraints = false
            
                self.view.addSubview(jellyGifImageView)
                NSLayoutConstraint.activate([
                        jellyGifImageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
                        jellyGifImageView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
                        jellyGifImageView.widthAnchor.constraint(equalToConstant: self.view.frame.width),
                        jellyGifImageView.heightAnchor.constraint(equalTo: jellyGifImageView.widthAnchor, multiplier: 150/250)
                    ])
                jellyGifImageView.startGif(with: .localPath(gifUrl))
            }
        }
    }
    
    func httpRequest(completionBlock: @escaping (String) -> Void) -> Void {
        let url = URL(string: "https://api.giphy.com/v1/gifs/random?api_key=your_awesome_api_key_here")!
        
        URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
            guard let data = data, error == nil else { return }

            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
                let images = (json?["data"] as! [String: Any])["images"] as! [String: Any]
                let url = (images["fixed_width"] as! [String: Any])["url"] as! String
                
                completionBlock(url)
            } catch {
                print("error")
            }
        }).resume()
    }
}
